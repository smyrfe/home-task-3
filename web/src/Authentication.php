<?php

namespace src;


class Authentication
{
    //Проверка пароля
    static function verifyPassword($userName, $password)
    {
        $search_file = Accounts::getListAccounts();
        if (in_array($userName, $search_file)) {
            $data = file_get_contents("../../userData/json/accounts/" . $userName . ".json");
            $data_decode = json_decode($data, true);
            if (password_verify($password, $data_decode["Password"])) {
                return new User($userName);
            } else {
                return "Invalid password";
            }
        } else {
            return "Account not found";
        }
    }
}
