<?php

if (isset($_POST['login'])){
    header ("Location: authentication_handler.php");
} elseif (isset($_POST['registration'])){
    header ("Location: registration_page.php");
} elseif (isset($_POST['changeInfo'])){
    header ("Location: change_personal_info.php");
} elseif (isset($_POST['logout'])) {
    header("Location: destroy_session.php");
} elseif (isset($_POST['addNewUser'])){
    header("Location: registration_page.php");
} elseif (isset($_POST['mainPage'])){
    header("Location: ../index.php");
}