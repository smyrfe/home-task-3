<?php
require'startup.php';
require_once "../common.php";
    if (isset($_SESSION['invalidPassword'])) {
        echo "<h2>" . "Извините, введен неверный пароль для пользователя " . $_SESSION['userName'] . "</h2>";
    } elseif (isset($_SESSION['accountNotFound'])) {
        echo "<h2>" . "Извините, пользователь " . $_SESSION['userName'] . " не зарегистрирован" . "</h2>";
    } elseif (isset($_SESSION['emptyValues'])){
        echo "<h2>" . "Заполните все поля ввода" . "</h2>";
    }
    ?>

<form method="POST" action="destroy_session.php">
    <button type="submit">На главную</button>
</form>
