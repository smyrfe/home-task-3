<?php

namespace src;

class User
{
    public  $FirstName;

    public  $LastName;

    public  $City;

    public  $Email;

    public  $Password;

    public  $Sex;

    public  $Role;

    //Метод получения личной иномарции о пользотвателе
    final function getPersonalInfo ($userName)
    {
        $search_file = Accounts::getListAccounts();
        if (in_array($userName, $search_file)) {
            $data = file_get_contents("../../userData/json/accounts/" . $userName . ".json"); //открываем json файл
            $data_decode = json_decode($data, TRUE); //декодируем его
            foreach ($data_decode as $key => $value) {
                switch ($key) {
                    case "FirstName":
                        $this->FirstName = $value;
                        break;
                    case "LastName":
                        $this->LastName = $value;
                        break;
                    case "City":
                        $this->City = $value;
                        break;
                    case "Email":
                        $this->Email = $value;
                        break;
                    case "Password":
                        $this->Password = $value;
                        break;
                    case "Sex":
                        $this->Sex = $value;
                        break;
                    case "Role":
                        $this->Role = $value;
                        break;
                }
            }
        }
    }
    // Выводит личную информацию о пользователе
    final function showPersonalInfo ($userName)
    {
        echo "<h4>" . "Имя:" . " " . $this->FirstName . "</h4>";
        echo "<h4>" . "Фамилия:" . " " . $this->LastName . "</h4>";
        echo "<h4>" . "Город:" . " " . $this->City . "</h4>";
        echo "<h4>" . "Е-mail:" . " " . $this->Email . "</h4>";
        echo "<h4>" . "Пол:" . " " . $this->Sex . "</h4>";
        echo "<h4>" . "Роль:" . " " . $this->Role . "</h4>";
    }

    function __construct($userName){
       $this->getPersonalInfo($userName);
    }
    //Метод для редактирования личной информации
    static function  changePersonalInfo ($userName)
    {
        $_SESSION['newUserName'] = strtolower($_POST['newEmail']); //переводим в ниж.регистр новое имя пользователя
        $dir = "../../userData/json/accounts/"; //Директория в которой хранятся Json файлы
        $oldPath = $dir . $_SESSION['userName'] . ".json"; //Путь к файлу с текущим именем пользователя
        $newPath = $dir . $_SESSION['newUserName'] . ".json"; //Путь к файлу с изменненым именем пользователя

        //Массив с обновленными данными о пользователе
        $newInformation = array(
            'FirstName' => $_POST['newFirstName'],
            'LastName'  => $_POST['newLastName'],
            'City'      => $_POST['newCity'],
            'Email'     => $_POST['newEmail'],
            'Password'  => password_hash($_POST['newPassword'], PASSWORD_DEFAULT),
            'Sex'       => $_POST['Sex'],
        );

        $search_file = Accounts::getListAccounts();

        if (in_array($_SESSION['userName'], $search_file)) {
            $data = file_get_contents($dir . $_SESSION['userName'] . ".json"); //открываем файл с json строкой
            $data_decode = json_decode($data, true);
            $data_decode = array_replace($data_decode, $newInformation);
            file_put_contents($oldPath, json_encode($data_decode));
            rename($oldPath, $newPath); //переименовываем имя файла в соотв. с новым именем пользователя
            $_SESSION['userName'] = $_SESSION['newUserName'];
        }
    }

    public function __toString()
    {
        return $this->Email;
    }

}