<?php
include 'startup.php';
require_once '../common.php';

 if (isset($_POST['newFirstName']) && isset($_POST['newLastName']) && isset($_POST['newCity'])
     && strlen($_POST['newEmail']) > 0 && strlen($_POST['newPassword']) > 0 && isset($_POST['Sex'])) {
     src\User::changePersonalInfo($_SESSION['userName']);
     header("Location: login.php");
 }  else {
     header ("Location: error_change_personal_info.php");
 }

 if (isset($_POST['back'])){
     header ("Location: login.php");
 }
