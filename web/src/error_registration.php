<?php
session_start();
if (isset($_SESSION['unique']) && !$_SESSION['unique']) {
    echo "<h2>" . "Ошибка при регистрации!" . " " . "Пользователь с таким именем уже существует" . "</h2>";
    session_destroy();
} elseif (isset($_SESSION['emptyValues']) && $_SESSION['emptyValues'] == "Empty values"){
    echo "<h2>" . "Ошибка при регистрации!" . " " . "Вы не указали E-mail или пароль" . "</h2>";
    session_destroy();
}
?>
<title>Ошибка при регистрации</title>
<form method="POST" action="handler.php">
    <button type="submit" name="registration" >Вернуться на страницу регистрации</button>
    <button type="submit" name="mainPage">На главную</button>
</form>
