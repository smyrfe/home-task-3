<?php

namespace src;


class Registration
{   //Проверка на уникальность ЛогинНейма
    public static function checkUniquenessUser($userName)
    {
        $search_file = Accounts::getListAccounts();
        if(in_array($userName, $search_file)){
            return false;
        } else {
            return true;
        }
    }
    //Регистрация нового участника
    public static function regNewMember($userName, $password)
    {
        if (strlen($_POST['Email']) > 0 && strlen($_POST['Password']) > 0) {
            $loginName = mb_strtolower($_POST['Email']); //Переводим строку с  логином в ниж. регистр
            $_SESSION['unique'] = self::checkUniquenessUser($loginName); //Проверяем на уникальность Email
            if ($_SESSION['unique']) {
                $path = "../../userData/json/accounts/" . $loginName . ".json"; //Путь к папке с аккаунтами
                /*Проверяем авторизировался ли пользователь, если да и он является админом, то по ключу 'Role'
                * будет передаваться выбранное им значение, а если нет, то по умолчанию присвается Member
                */
                if (isset($_SESSION['userName']) && $_SESSION['Role'] === "Admin") {
                    $newMember = array(
                        'FirstName' => $_POST['FirstName'],
                        'LastName'  => $_POST['LastName'],
                        'City'      => $_POST['City'],
                        'Email'     => $_POST['Email'],
                        'Password'  => password_hash($_POST['Password'],PASSWORD_DEFAULT),
                        'Sex'       => $_POST['Sex'],
                        'Role'      => $_POST['Role']
                    );
                    file_put_contents($path, json_encode($newMember));
                    header("Location: ../index.php");
                } else {
                    $newMember = array(
                        'FirstName' => $_POST['FirstName'],
                        'LastName'  => $_POST['LastName'],
                        'City'      => $_POST['City'],
                        'Email'     => $_POST['Email'],
                        'Password'  => password_hash($_POST['Password'],PASSWORD_DEFAULT),
                        'Sex'       => $_POST['Sex'],
                        'Role'      => $_POST['Role']
                    );
                    file_put_contents($path, json_encode($newMember));
                    header("Location: ../index.php");
                }
            } else {
                header("Location: error_registration.php");
            }
        } else {
            $_SESSION['emptyValues'] = "Empty values";
            header("Location: error_registration.php");
        }

    }
}