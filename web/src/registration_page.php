<?php
require("handler.php");
require "../common.php";
?>
<title>Регистрация нового пользователя</title>
<link  rel="stylesheet" type="text/css"  href="../css/style.css">
<h1>Регистрация нового пользователя</h1>
<form method="POST" action="registration_handler.php">
    <div class="main">
        <div class="field">
            <label>Имя:</label>
            <input type="text" name="FirstName"><br>
            <br><label>Фамилия:</label>
            <input type="text" name="LastName"><br>
            <br><label>Город:</label>
            <input type="text" name="City"><br>
            <br><label>E-mail:</label>
            <input type="text" name="Email"><br>
            <br><label>Пароль:</label>
            <input type="password" name="Password"><br>
            <br><label>Роль:</label>
            <select name="Role">
                <option value="Member">Участник</option>
                <?php if (isset($_SESSION['userName']) && $_SESSION['Role'] === "Admin") { ?>
                <option value="Admin">Администратор</option>
                <?php } ?>
            </select><br>
            <br><label>Пол:</label>
            <select name="Sex">
                <option value="Male">Мужской</option>
                <option value="Female">Женский</option>
            </select><br>
        </div>
        <br><button type="submit" name="addUser">Зарегистрировать</button>
        <button type="submit" name="mainPage">На главную</button>
    </div>
</form>



