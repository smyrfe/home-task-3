<?php

namespace src;


class Accounts
{

    public static function getListAccounts()  // Метод, который возвращает список аккаунтов
    {
        $pathFile = "../../userData/json/accounts/";
        $search_file = glob($pathFile . "*.json"); //получаем список файлов с расширением .json
        $search_file = str_replace($pathFile, "", $search_file);// удаляем директорию из строки
        $search_file = str_replace(".json", "", $search_file); //удаляем расширение
        return $search_file;
    }
    //Показать список аккаунтов
    public static function showAllAccounts()
    {
        $search_file = self::getListAccounts();
        foreach ($search_file as $key => $value) {?>
            <form method="POST" action="about_user.php">
                <table>
                    <tr>
                        <td>Имя пользователя</td>
                        <td>Роль</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td><?php echo $value;?></td>
                        <td><?php echo $_SESSION['Role'];?></td>
                        <td><button type="submit" name="user" value="<?php echo $value?>">О пользователе</button></td>
                    </tr>
                </table>
            </form>
      <?php  }

    }


}
