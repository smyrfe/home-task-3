<?php

require '../common.php';

if (isset($_POST['login'])) {
    if (strlen($_POST['userName']) > 0 && strlen($_POST['Password']) > 0) {
        $currentUser = src\Authentication::verifyPassword($_POST['userName'], $_POST['Password']);
        if (gettype($currentUser) == "object") {
            $_SESSION['userName'] = $currentUser->Email;
            $_SESSION['Password'] = $currentUser->Password;
            $_SESSION['Role'] = $currentUser->Role;
            header("Location: login.php");
        } elseif ($currentUser === "Invalid password") {
            $_SESSION['userName'] = $_POST['userName'];
            $_SESSION['invalidPassword'] = "Invalid password";
            header("Location: error_authentication.php");
        } elseif ($currentUser === "Account not found") {
            $_SESSION['userName'] = $_POST['userName'];
            $_SESSION['accountNotFound'] = "Account not found";
            header("Location: error_authentication.php");
        }
    } else {
        $_SESSION['emptyValues'] = "Empty values";
        header("Location: error_authentication.php");
    }
} elseif (isset($_POST['registration'])) {
    header("Location: registration_page.php");
}
