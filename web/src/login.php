<?php
require "startup.php";
require "../common.php";

$currentUser = new \src\User($_SESSION['userName']);

 if (isset($_SESSION['userName'])) {
     echo "<h2>" . 'Здравствуйте, ' . $currentUser . "," . " " . "ваша роль -" . " " . $_SESSION['Role'] . "</h2>" . "<hr>";
 } else {
     header('Location: ../index.php');
    }
?>
<?php //Если текущ. пользователь админ, то выводим список пользователей в системе
if ($_SESSION['Role'] === "Admin" ){ ?>
    <h3>Список пользователей в системе</h3>
    <?php \src\Accounts::showAllAccounts();} ?>

<hr>
<title>Добро пожаловать!</title>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eu sapien vitae nisi eleifend convallis quis sit amet tellus.
    Fusce imperdiet, metus ac rutrum scelerisque, dolor
    ligula posuere sapien, ut iaculis urna odio non sem. Ut semper maximus nulla, eget ultricies purus
    fringilla quis. Maecenas nisl nisi, tincidunt id purus ac,
    convallis sollicitudin lacus. Phasellus et tincidunt tellus. Etiam mollis dolor nibh,
    quis mollis mi convallis quis. Praesent non leo rhoncus, vestibulum ante ut, sodales velit.</p>
<form method="POST" action="handler.php">
    <?php if($_SESSION['Role'] === "Admin"){?>
        <button type="submit" name="addNewUser">Добавить нового пользователя</button>
    <?php }?>
    <button type="submit" name="changeInfo">Изменить профиль</button>
    <button type="submit" name="logout">Выход</button>
</form>


